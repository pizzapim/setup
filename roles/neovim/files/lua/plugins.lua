return require('packer').startup(function()
	--- Packer can manage itself
	use 'wbthomason/packer.nvim'

	-- Colorscheme
	use 'gruvbox-community/gruvbox'

	-- Better increment/decrement
	use 'monaqa/dial.nvim'

	-- Incredibly cute duck
	use 'tamton-aquib/duck.nvim'

	-- Easy LSP configurations
	use 'neovim/nvim-lspconfig'

	-- Git line decorations
	use 'lewis6991/gitsigns.nvim'

	-- File browser
	use {
		"nvim-neo-tree/neo-tree.nvim",
		branch = "v2.x",
		requires = {
			"nvim-lua/plenary.nvim",
			"nvim-tree/nvim-web-devicons",
			"MunifTanjim/nui.nvim",
		}
	}

	-- Syntax highlighting
	use {
		'nvim-treesitter/nvim-treesitter',
		run = function()
		local ts_update = require('nvim-treesitter.install').update({ with_sync = true })
			ts_update()
		end,
	}

	-- Fast jumping
	use 'ggandor/leap.nvim'

	-- Surround objects
	use({
		"kylechui/nvim-surround",
		tag = "*",
		requires = {
			"tpope/vim-repeat"
		}
	})

	-- Commenting
	use 'terrortylor/nvim-comment'

	-- Automatic settings depending on file type
	use 'tpope/vim-sleuth'
end)
