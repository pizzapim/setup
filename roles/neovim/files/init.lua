require('plugins')

vim.g.mapleader = ";"
vim.o.signcolumn = "yes"
vim.wo.number = true
vim.wo.relativenumber = true
vim.wo.cursorline = true
