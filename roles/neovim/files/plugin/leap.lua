require('leap').add_default_mappings()

-- Don't remap 'x' in visual mode.
vim.keymap.del({'x', 'o'}, 'x')
vim.keymap.del({'x', 'o'}, 'X')
