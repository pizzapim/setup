-- Change increment/decrement key mappings
vim.api.nvim_set_keymap("n", "<C-a>", require("dial.map").inc_normal(), {noremap = true})
vim.api.nvim_set_keymap("n", "<C-x>", require("dial.map").dec_normal(), {noremap = true})
vim.api.nvim_set_keymap("v", "<C-a>", require("dial.map").inc_visual(), {noremap = true})
vim.api.nvim_set_keymap("v", "<C-x>", require("dial.map").dec_visual(), {noremap = true})
vim.api.nvim_set_keymap("v", "g<C-a>", require("dial.map").inc_gvisual(), {noremap = true})
vim.api.nvim_set_keymap("v", "g<C-x>", require("dial.map").dec_gvisual(), {noremap = true})

local augend = require("dial.augend")
require("dial.config").augends:register_group{
	default = {
		augend.integer.alias.hex,
		augend.date.alias["%m/%d/%Y"],
		augend.integer.alias.decimal_int,
		augend.constant.alias.bool,
		augend.constant.alias.Alpha,
		augend.constant.alias.alpha,
		augend.constant.new{
			elements = {"&&", "||"},
			word = false,
			cyclic = true,
		},
		augend.constant.new{
			elements = {"Monday", "Tuesday", "Wednesday",
			  	    "Thursday", "Friday", "Saturday", "Sunday"},
			word = true,
			cyclic = true,
		},
		augend.constant.new{
			elements = {"Maandag", "Dinsdag", "Woensdag", "Donderdag", "Vrijdag", "Zaterdag", "Zondag"},
			word = true,
			cyclic = true,
		},
		augend.constant.new{
			elements = {"maandag", "dinsdag", "woensdag", "donderdag", "vrijdag", "zaterdag", "zondag"},
			word = true,
			cyclic = true,
		},
	},
}
