# Personal Linux Setup

My current setup is [Regolith OS](https://regolith-desktop.com/) which is basically Ubuntu + i3.

## Before installing

- Put asymmetric key pair in ~/.ssh
- Install git
- Install Ansible: https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html

## TODO

- Telegram (maybe only possible to download)
- thunderbird config
- some kind of tag setup with: bootstrap, update, etc.
- i3 app shortcuts
- Configuration for desktop at uni. It should be really similar, except syncthing should have a new keypair. Also some applications are not necessary, e.g. nicotine+ and virtualbox.
- I think fzf is not cloned?

Would like Ansible Vault to check in keypair for syncthing.
However, then I would need a password to unlock the vault.
As it is publicly available, password needs to be long and strong.
I can just put it in my keepass, but can be annoying to copy every time.
Therefore, I could make this available using Secret Service and write a script to fetch it.
However, keepass cannot run secret service as Ubuntu already runs one.
But if I disable that, my SSH agent does not work anymore.
As an intermediate solution, I can just put the password in keepass...

#### Neovim

- More cool plugins :)
- dropdown autocomplete
- fzf in vim? telescope or something

## Long-term TODO

- Move away from GNOME because it is quite slow and configuration is pretty bad. This would also mean moving away from Regolith unfortunately. I would probably move to KDE with i3. Maybe there is some distro that does this OOTB?
- Move away from systemd. Not sure how feasibly that is nowadays, but I just get annoyed by some systemd "features" such as resolved.
